import { Logger, logger } from '@geeebe/logging';
import { compose, Middleware, ReceiveQueue, RequestHandler, SendQueue } from '@geeebe/queue';
import { Service } from '@geeebe/service';
import { Job, JobsOptions, Queue, QueueScheduler, Worker, WorkerOptions } from 'bullmq';
import * as Redis from 'ioredis';
import { jobTiming } from './prometheus';

export interface SendBullMqOptions {
  queueName: string;
}

const isCluster = (redis: Redis.Cluster | Redis.Redis): redis is Redis.Cluster => {
  return typeof (redis as Redis.Cluster).nodes === 'function';
};

export interface BullMqOptions {
  logger?: Logger;
  redis: () => Redis.Redis | Redis.Cluster;
}

export abstract class BullMq<Options extends BullMqOptions> implements Service {
  public get isConnected(): boolean {
    if (!this.redis) return false;
    if (isCluster(this.redis)) {
      return this.redis.nodes().some((node) => node.status === 'ready');
    }
    return this.redis.status === 'ready';
  }

  protected readonly logger: Logger;
  protected redis: Redis.Cluster | Redis.Redis | null = null;
  protected state: 'stopped' | 'starting' | 'started' | 'stopping' = 'stopped';

  constructor(
    protected readonly options: Options,
  ) {
    this.logger = this.options.logger ?? logger.child({ module: 'bullmq-send' });
  }

  public async start(): Promise<void> {
    if (this.state === 'starting' || this.state === 'started') throw new Error(`Already ${this.state}`);
    this.state = 'starting';

    this.redis = this.options.redis();
    this.redis.on('connect', this.onConnect);
    this.redis.on('ready', this.onReady);
    this.redis.on('reconnecting', this.onReconnecting);
    this.redis.on('end', this.onEnd);

    await this.startImpl();

    this.logger('Started');
    this.state = 'started';
  }

  public async stop(): Promise<void> {
    if (this.state === 'stopping' || this.state === 'stopped' || !this.redis) throw new Error(`Already ${this.state}`);
    this.state = 'stopping';

    await this.stopImpl();

    this.redis.off('connect', this.onConnect);
    this.redis.off('ready', this.onReady);
    this.redis.off('reconnecting', this.onReconnecting);
    this.redis.off('end', this.onEnd);
    this.redis = null;

    this.logger('Stopped');
    this.state = 'stopped';
  }

  protected abstract startImpl(): Promise<void>;

  protected abstract stopImpl(): Promise<void>;

  private onConnect = (data: {}): void => {
    this.logger('Redis connect', data);
  }

  private onReady = (data: {}): void => {
    this.logger('Redis ready', data);
  }

  private onReconnecting = (data: {}): void => {
    // connectionFailures.inc({ kind: 'closed' });
    this.logger('Redis reconnecting', data);
  }

  private onEnd = (data: {}): void => {
    this.logger('Redis end', data);
  }
}

export class ReceiveBullMq extends BullMq<BullMqOptions> implements ReceiveQueue<WorkerOptions, Job> {
  private queues: Worker[] = [];
  private readonly middleware: Middleware<Job>[] = [];

  protected startImpl(): Promise<void> {
    return Promise.resolve();
  }

  protected async stopImpl(): Promise<void> {
    await Promise.all(
      this.queues.map((queue) => queue.close()),
    );
  }

  public listen(queueName: string, options?: WorkerOptions): Promise<void> {
    const handler = compose<Job>(this.middleware);
    const queue = new Worker(queueName, this.handleMessage(handler), { connection: this.redis, ...options });

    this.queues.push(queue);
    return Promise.resolve();
  }

  public use(middleware: Middleware<Job>): void {
    this.middleware.push(middleware);
  }

  private handleMessage = (handler: RequestHandler<Job>) =>
    (job: Job): Promise<void> => {
      const endTimer = jobTiming.startTimer();
      return new Promise((resolve, reject) => {
        handler(job, (err) => {
          endTimer({ status: err ? 'error' : 'ok' });
          // TODO are retries handled?
          if (!err) {
            return resolve();
          }
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { data, ...meta } = job;
          this.logger.error(err ?? new Error('Unknown error'), { job: meta });
          return reject(err);
        });
      });
    }
}

export class SendBullMq extends BullMq<SendBullMqOptions & BullMqOptions> implements SendQueue<string, JobsOptions> {
  private queue: Queue;

  protected startImpl(): Promise<void> {
    this.queue = new Queue(this.options.queueName, { connection: this.redis });
    return Promise.resolve();
  }

  protected async stopImpl(): Promise<void> {
    await this.queue.close();
  }

  public async send(target: string, message: {}, options?: JobsOptions): Promise<void> {
    await this.queue.add(target, message, options);
  }
}

export class BullMqScheduler extends BullMq<SendBullMqOptions & BullMqOptions> {
  private scheduler: QueueScheduler;

  protected startImpl(): Promise<void> {
    this.scheduler = new QueueScheduler(this.options.queueName, { connection: this.redis });
    return Promise.resolve();
  }

  protected async stopImpl(): Promise<void> {
    await this.scheduler.close();
  }
}

