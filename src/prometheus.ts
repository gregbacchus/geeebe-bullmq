import { Counter, Summary } from 'prom-client';

/**
 * Custom prometheus metric for counting number of RabbitMQ connection failures and/or disconnects
 */
export const connectionFailures = new Counter({
  help: 'Number of connection failures to from BullMQ',
  labelNames: ['kind'],
  name: 'bullmq_client_failures',
});

export const jobTiming = new Summary({
  help: 'BullMQ job processing timing (seconds)',
  labelNames: ['status'],
  name: 'bullmq_job',
});
